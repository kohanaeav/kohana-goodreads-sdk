<?php defined('SYSPATH') or die('No direct script access.');

class Kohana_OAuth_Provider_Goodreads extends OAuth_Provider {

	public $name = 'goodreads';

	protected $signature = 'HMAC-SHA1';

	public function url_request_token()
	{
		return 'http://www.goodreads.com/oauth/request_token';
	}

	public function url_authorize()
	{
		return 'http://www.goodreads.com/oauth/authorize';
	}

	public function url_access_token()
	{
		return 'http://www.goodreads.com/oauth/access_token';
	}
//
//	public function request_token(OAuth_Consumer $consumer, array $params = NULL)
//	{
//		if ( ! isset($params['scope']))
//		{
//			// All request tokens must specify the data scope to access
//			// http://code.google.com/apis/accounts/docs/OAuth.html#prepScope
//			throw new Kohana_OAuth_Exception('Required parameter to not passed: :param', array(
//				':param' => 'scope',
//			));
//		}
//
//		return parent::request_token($consumer, $params);
//	}

} // End OAuth_Provider_Google
