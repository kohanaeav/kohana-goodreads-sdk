<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class OAuth_Request extends Kohana_OAuth_Request {

	/**
	 * Sign the request, setting the `oauth_signature_method` and `oauth_signature`.
	 *
	 * @param   OAuth_Signature  signature
	 * @param   OAuth_Consumer   consumer
	 * @param   OAuth_Token      token
	 * @return  $this
	 * @uses    OAuth_Signature::sign
	 */
	public function sign(OAuth_Signature $signature, OAuth_Consumer $consumer, OAuth_Token $token = NULL)
	{
		if ( ! is_null($token))
		{
			$this->param('oauth_token', $token);
			if ($this->name == "request")
			{
				$this->param("oauth_callback", $consumer->callback);
			}
		}
		else
		{
			return parent::sign($signature, $consumer);
		}

		return $this;
	}

}
