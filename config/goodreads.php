<?php

defined('SYSPATH') OR die('No direct access allowed.');

return array
	(
	'key' => 'application key from goodreads',
	'secret' => 'application secret from goodreads',
);